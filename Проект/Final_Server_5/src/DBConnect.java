import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

import java.sql.*;
import java.net.*;
import java.io.*;
import java.util.concurrent.ThreadLocalRandom;

public class DBConnect extends Thread {
    
    protected Socket socket;
    private int client;
    
    public DBConnect(Socket s, int z) {
        this.socket = s;
        this.client = z;
    }
    
    public void run() {
        Connection con;
        Statement st, st2;
        InputStream sin;
        OutputStream sout;
        DataInputStream in = null;
        DataOutputStream out = null;
        int action_code, op = client;
        
        try {
            sin = socket.getInputStream();
            sout = socket.getOutputStream();
            in = new DataInputStream(sin);
            out = new DataOutputStream(sout);
        } catch (Exception ex) {
            System.out.println("Input/Output connection failure.");
        }
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bills?characterEncoding=Cp1251", "root", "");
            st = con.createStatement();
            st2 = con.createStatement();
        } catch (java.lang.ClassNotFoundException | java.sql.SQLException opp) {
            System.out.println("Data base error: " + opp);
            try {
                out.writeInt(97);
                out.flush();
            } catch (Exception e) {
                System.out.println("Server - Client connection error: " + e);
            }
            return;
            
        }
        
        while (true) {
            try {
                action_code = in.readInt();
            } catch (Exception e) {
                System.out.println("Client №" + op + " disconnected.");
                return;
            }
            
            switch (action_code) {
                case 1:
                    check(st, in, out, op);
                    break;
                case 2:
                    registration(st, in, out, con, op);
                    break;
                case 3:
                    put(st, in, out, con, op);
                    break;
                case 8:
                    delete(st, in, out,con, op);
                    break;
                case 5:
                    all(st, st2, in, out, op);
                    break;
                case 6:
                    Details(st, in, out, con, op);
                    break;
                case 9:
                    System.out.println("Client №" + op + " disconnected.");
                    return;
            }
        }
        
    }
    
    public void check(Statement st, DataInputStream in, DataOutputStream out, int cl) {
        try {
            String query = "Select * from persons";
            ResultSet rs = st.executeQuery(query);
    
            boolean log = false;
            String login_in = in.readUTF(), password_in = in.readUTF();
            System.out.println("№" + cl + ": Got login: " + login_in + "  password: " + password_in);
            System.out.println("№" + cl + ": Searching user...");
            rs.beforeFirst();
    
            while (rs.next()) {
                String login_out = rs.getString("login");
                String password_out = rs.getString("password");
                if (login_out.equals(login_in) && password_out.equals(password_in)) {
                    System.out.println("№" + cl + ": Found user!");
                    
                    log = true;
                    out.writeInt(1);
                    out.flush();
                    break;
                }
            }
    
            if (!log) {
                System.out.println("№" + cl + ": User not found.");
                out.writeInt(-1);
                out.flush();
            }
    
    
        } catch (Exception ex) {
            System.out.println("№" + cl + ": Server - Client connection error: " + ex);
            try {
                out.writeInt(97);
                out.flush();
            } catch (Exception e) {
                System.out.println("№" + cl + ": Server - Client connection error: " + e);
            }
        }
    }
    
    public void registration(Statement st, DataInputStream in, DataOutputStream out, Connection con, int cl) {
        try {
            String query = "Select * from persons";
            ResultSet rs = st.executeQuery(query);
    
            String first_name = in.readUTF(), second_name = in.readUTF(), phone_number = in.readUTF();
            String login = in.readUTF(), password = in.readUTF(), email = in.readUTF();
            System.out.println("№" + cl + ": Got registration information.");
    
            rs.beforeFirst();
    
            while (rs.next()) {
                String login_out = rs.getString("login");
                if (login_out.equals(login)) {
                    System.out.println("№" + cl + ": Login already used.");
                    out.writeInt(5);
                    out.flush();
                    return;
                }
            }
    
            int id = 0;
            rs.beforeFirst();
            while (rs.next()) {
                id = rs.getInt("id");
            }
            id++;
    
            String sql = "INSERT INTO persons (id,first_name,second_name,phone_number,login,password,email)" +
                    "VALUES (?,?,?,?,?,?,?)";
    
            PreparedStatement pre_st = con.prepareStatement(sql);
            pre_st.setInt(1, id);
            pre_st.setString(2, first_name);
            pre_st.setString(3, second_name);
            pre_st.setString(4, phone_number);
            pre_st.setString(5, login);
            pre_st.setString(6, password);
            pre_st.setString(7, email);
    
            pre_st.execute();
    
            query = "Select * from acc";
            rs = st.executeQuery(query);
            
            id = 0;
            rs.beforeFirst();
            while (rs.next()) {
                id = rs.getInt("id");
            }
            id++;
    
            sql = "INSERT INTO acc (id,login)" + "VALUES (?,?)";
    
            pre_st = con.prepareStatement(sql);
            pre_st.setInt(1, id);
            pre_st.setString(2, login);
    
            pre_st.execute();
            
            
            
            Statement s = con.createStatement();
            s.execute("ALTER TABLE `checks` ADD `"+login+"` VARCHAR(30) NOT NULL DEFAULT ''");
    
            
            query = "UPDATE `checks` SET `"+login+"` = '" + phone_number + "' WHERE `checks`.`id` = 1";
            pre_st = con.prepareStatement(query);
            pre_st.execute();
            
            
            System.out.println("№" + cl + ": Registration done");
            out.writeInt(1);
            out.flush();
    
        } catch (Exception ex) {
            System.out.println("№" + cl + ": Server - Client connection error: " + ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№" + cl + ": Server - Client connection error: " + e);
            }
        }
    }
    
    public void delete(Statement st, DataInputStream in, DataOutputStream out, Connection con, int cl) {
        try {
            String login = in.readUTF(), bill = in.readUTF();
            
            String query = "Select * from checks";
            ResultSet rs = st.executeQuery(query);
            PreparedStatement pre_st;
    
            System.out.println("№" + cl + ": Deleting the check...");
    
            int id = 0;
            rs.beforeFirst();
            while (rs.next()) {
                id = rs.getInt("id");
                if (rs.getString(login).equals(bill)){
                    break;
                }
            }
    
            rs.beforeFirst();
            while (rs.next()) {
                String bill_out = rs.getString(login);
                if (bill_out.equals(bill)) {
                    query = "UPDATE `checks` SET `"+login+"` = '' WHERE `checks`.`id` = "+ id;
                    pre_st = con.prepareStatement(query);
                    pre_st.execute();
    
                    out.writeInt(1);
                    System.out.println("№" + cl + ": Check deleted.");
                    out.flush();
                    return;
                }
            }
    
            System.out.println("№" + cl + ": Check not found.");
            out.writeInt(-2);
            out.flush();
    
        } catch (Exception ex) {
            System.out.println("№" + cl + ": Server - Client connection error: " + ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№" + cl + ": Server - Client connection error: " + e);
            }
        }
    }
    
    public void put(Statement st, DataInputStream in, DataOutputStream out, Connection con, int cl) {
        try {
            int money = in.readInt();
            
            String query = "Select * from acc", login = in.readUTF(), type = in.readUTF() + "_money";
            ResultSet rs = st.executeQuery(query);
            PreparedStatement pre_st;
            
            System.out.println("№" + cl + ": Sending money...");
    
            rs.beforeFirst();
            while (rs.next()) {
                String login_out = rs.getString("login");
                if (login_out.equals(login)) {
                    out.writeInt(1);
                            query = "UPDATE `acc` SET `"+ type +"` = '" + money + "' WHERE `acc`.`id` = " + rs.getInt("id");
                            pre_st = con.prepareStatement(query);
                            pre_st.execute();

    
                    System.out.println("№" + cl + ": Money send.");
                    out.flush();
                    return;
                }
            }
            
            System.out.println("№" + cl + ": Sending problem.");
            out.writeInt(-2);
            out.flush();
            
        } catch (Exception ex) {
            System.out.println("№" + cl + ": Server - Client connection error: " + ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№" + cl + ": Server - Client connection error: " + e);
            }
        }
    }
    
    public void all(Statement st, Statement st2, DataInputStream in, DataOutputStream out, int cl) {
        try {
            String query = "Select * from checks", login = in.readUTF();
            ResultSet rs = st.executeQuery(query);
    
            System.out.println("№" + cl + ": Searching account details...");
            
            int number = 0;
            rs.beforeFirst();
            while (rs.next()){
                if (!rs.getString(login).isEmpty()){
                    number++;
                }
            }
            
            
            out.writeInt(number);
            rs.beforeFirst();
            while (rs.next()) {
                if (!rs.getString(login).isEmpty()) {
                    String name = rs.getString("Вид");
                    out.writeUTF(name);
                    out.writeUTF(rs.getString(login));
                    name = name + "_money";

                    ResultSet rs2 = st2.executeQuery("Select * from acc");
                    
                    rs2.beforeFirst();
                    while (rs2.next()){
                        if (rs2.getString("login").equals(login)){
                            out.writeUTF(rs2.getString(name));
                            rs2.close();
                            break;
                        }
                    }
                }
            }
                    System.out.println("№" + cl + ": Account details send.");
                    out.flush();
                    return;
            
        } catch (Exception ex) {
            System.out.println("№" + cl + ": Server - Client connection error: " + ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№" + cl + ": Server - Client connection error: " + e);
            }
        }
    }
    
    public void Details(Statement st, DataInputStream in, DataOutputStream out, Connection con, int cl) {
        try {
            String query, sql, login = in.readUTF(), bill = in.readUTF(), acc = in.readUTF();
            ResultSet rs;
            PreparedStatement pre_st;
    
            System.out.println("№" + cl + ": Creating new account details...");
    
    
            query = "Select * from checks";
            rs = st.executeQuery(query);
    
            int id = 0;
            boolean t = false;
            rs.beforeFirst();
            while (rs.next()) {
                id = rs.getInt("id");
                if (rs.getString("Вид").equals(acc)){
                    t =true;
                    break;
                }
            }
    
            if (!t) {
                id++;
                sql = "INSERT INTO checks (id,Вид," + login + ")" + "VALUES (?,?,?)";
    
                pre_st = con.prepareStatement(sql);
                pre_st.setInt(1, id);
                pre_st.setString(2, acc);
                pre_st.setString(3, bill);
    
                pre_st.execute();
            }
            else{
                query = "UPDATE `checks` SET `"+login+"` = '" + bill + "' WHERE `checks`.`id` = "+ id;
                pre_st = con.prepareStatement(query);
                pre_st.execute();
            }
            
            
            
            try {
                Statement s = con.createStatement();
                s.execute("ALTER TABLE `acc` ADD `" + acc + "_money` INT NOT NULL DEFAULT '0'");
            }catch (MySQLSyntaxErrorException a){
                System.out.println("№" + cl + ": Table already has same acc type. ");
            }
            
            
            
            out.writeInt(1);
            System.out.println("№" + cl + ": Details added.");
            out.flush();
            return;
    
        } catch (Exception ex) {
            System.out.println("№" + cl + ": Server - Client connection error: " + ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№" + cl + ": Server - Client connection error: " + e);
            }
        }
    }
    
}