-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Час створення: Лис 20 2017 р., 16:25
-- Версія сервера: 10.1.26-MariaDB
-- Версія PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `bills`
--

-- --------------------------------------------------------

--
-- Структура таблиці `acc`
--

CREATE TABLE `acc` (
  `id` int(11) NOT NULL,
  `login` varchar(20) NOT NULL,
  `Телефон_money` int(11) NOT NULL DEFAULT '0',
  `Телебачення_money` int(11) NOT NULL DEFAULT '0',
  `Комуналка_money` int(11) NOT NULL DEFAULT '0',
  `Інтернет_money` int(11) NOT NULL DEFAULT '0',
  `Суші_money` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп даних таблиці `acc`
--

INSERT INTO `acc` (`id`, `login`, `Телефон_money`, `Телебачення_money`, `Комуналка_money`, `Інтернет_money`, `Суші_money`) VALUES
(1, 'Pavlo', 76, 100, 623, 48, 0),
(2, 'Petro', 170, 23, 119, 154, 0),
(3, 'Andrew', 0, 0, 0, 0, 5445),
(4, 'Vetal', 0, 0, 0, 0, 0),
(5, 'Roman', 0, 0, 0, 0, 0),
(6, 'Ivan', 0, 0, 0, 0, 0),
(7, 'Vlad', 0, 0, 0, 0, 0),
(8, 'Edyard', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `checks`
--

CREATE TABLE `checks` (
  `id` int(11) NOT NULL,
  `Вид` varchar(30) CHARACTER SET cp1251 COLLATE cp1251_ukrainian_ci NOT NULL,
  `Pavlo` varchar(30) NOT NULL DEFAULT '',
  `Petro` varchar(30) NOT NULL DEFAULT '',
  `Andrew` varchar(30) NOT NULL DEFAULT '',
  `Vetal` varchar(30) NOT NULL DEFAULT '',
  `Roman` varchar(30) NOT NULL DEFAULT '',
  `Ivan` varchar(30) NOT NULL DEFAULT '',
  `Vlad` varchar(30) DEFAULT '',
  `Edyard` varchar(30) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп даних таблиці `checks`
--

INSERT INTO `checks` (`id`, `Вид`, `Pavlo`, `Petro`, `Andrew`, `Vetal`, `Roman`, `Ivan`, `Vlad`, `Edyard`) VALUES
(1, 'Телефон', '0675522111', '0674455111', '0681122333', '0675511888', '0675588999', '0674455111', '0675511495', '0654896546'),
(2, 'Телебачення', '455478562', '', '44548833', '', '6622458a', '56997755112', '', ''),
(3, 'Інтернет', '556644228877', '45421455', '4537868877', '', '465522113', '', '', ''),
(4, 'Комуналка', 'as547a22', '48846236', '', '', '746952314', '', '', ''),
(5, 'Суші', '', '', '468236941', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблиці `persons`
--

CREATE TABLE `persons` (
  `id` int(11) NOT NULL,
  `first_name` varchar(30) CHARACTER SET cp1251 COLLATE cp1251_ukrainian_ci NOT NULL,
  `second_name` varchar(30) CHARACTER SET cp1251 COLLATE cp1251_ukrainian_ci NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп даних таблиці `persons`
--

INSERT INTO `persons` (`id`, `first_name`, `second_name`, `phone_number`, `login`, `password`, `email`) VALUES
(1, 'Павло', 'Білий', '0675522111', 'Pavlo', 'pavlo', 'White@gmail.com'),
(2, 'Петро', 'Синій', '0674455111', 'Petro', 'petro', 'Blue@gmail.com'),
(3, 'Андрій', 'Биток', '0681122333', 'Andrew', 'andrew', 'Andrew@gmail.com'),
(4, 'Віталій', 'Спина', '0675511888', 'Vetal', 'vetal', 'Spuna@gmail.com'),
(5, 'Роман', 'Великий', '0675588999', 'Roman', 'roman', 'Big@gmail.com'),
(6, 'Іван', 'Чорний', '0674455111', 'Ivan', 'ivan', 'Ivan@gmail.com'),
(7, 'Влад', 'Сила', '0652211777', 'Vlad', 'vlad', 'Vladik@gmail.com'),
(8, 'Едуард', 'Едуард', '0654896546', 'Edyard', 'edyard', 'EKKDD@gmail.com');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `acc`
--
ALTER TABLE `acc`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `checks`
--
ALTER TABLE `checks`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
